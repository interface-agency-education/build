/* tslint:disable: variable-name max-line-length */
const { EXCLUDE_SOURCE_MAPS } = require('./../shared/constants');
const { ContextReplacementPlugin, DllPlugin, ProgressPlugin } = require('webpack');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');
const { root } = require('./../shared/helpers.js');

module.exports = {
    mode: 'none',
	devtool: 'source-map',
    module: {
        rules: [{
            test: /\.js$/,
            loader: 'source-map-loader',
            exclude: [EXCLUDE_SOURCE_MAPS],
        }, {
            test: /\.ts$/,
            loaders: [
                'awesome-typescript-loader',
                'angular2-template-loader',
                'angular-router-loader'
            ],
            exclude: [/\.(spec|e2e|d)\.ts$/],
        }, {
            type: 'javascript/auto',
            test: /\.json/,
            exclude: /node_modules/,
            use: [
                {
                    loader: 'file-loader',
                    options: { name: '[name].[ext]' },
                }
            ]
        },
        {
			test: /\.html/,
			loader: 'raw-loader',
			exclude: [/node_modules/, root('source/index.html')]
		}]
    },
    plugins: [
        new ProgressPlugin(),
        new FilterWarningsPlugin({
            exclude: /System\.import/,
        }),
        new ContextReplacementPlugin(
            /\@angular(\\|\/)core(\\|\/)fesm5/,
            root('client'), {}
        ),
        new DllPlugin({
            name: '[name]',
            path: root('dll/[name]-manifest.json')
        })
    ],
    resolve: {
        extensions: ['.ts', '.js', '.json']
    },
    entry: {
        polyfill: [
            '@angularclass/hmr',
            'ts-helpers',
            'zone.js',
            'core-js/client/shim.js',
            'core-js/es6/reflect.js',
            'core-js/es7/reflect.js',
            'querystring-es3',
            'strip-ansi',
            'url',
            'punycode',
            'events',
            'web-animations-js/web-animations.min.js',
            'webpack/hot/emitter.js',
            'zone.js/dist/long-stack-trace-zone.js'
        ],
        vendor: [
            '@angular/animations',
            '@angular/cdk',
            '@angular/common',
            '@angular/compiler',
            '@angular/core',
            '@angular/forms',
            '@angular/material',
            '@angular/platform-browser',
            '@angular/platform-browser-dynamic',
            '@angular/router',
            '@angularclass/hmr',
            '@angularclass/hmr-loader',
            'core-js',
            'rxjs',
            'web-animations-js',
            'zone.js'
        ]
    },
    output: {
        path: root('dll'),
        filename: '[name].dll.js',
        library: '[name]',
    },
    performance: {
        hints: false,
    }
};
